<?php
// $Id$

/**
 * @file
 *   This include processes Animoto.com video for use by the emfield.module
 */

define('EMVIDEO_ANIMOTO_MAIN_URL', 'http://animoto.com/');
define('EMVIDEO_ANIMOTO_PLAYER_URL', 'http://static.animoto.com/swf/w.swf?w=swf/vp1&d=33&m=b&r=w&i=m');
define('EMVIDEO_ANIMOTO_PLAYER_PAGE_BASE', 'https://animoto.com/play/');
define('EMVIDEO_ANIMOTO_PLAYER_PAGE_BASE_PATTERN', '//animoto.com/play/');
define('EMVIDEO_ANIMOTO_OEMBED_BASE', 'https://animoto.com/oembeds/create/?format=json');


/**
 * Implements hook emvideo_PROVIDER_info
 *
 * this returns information relevant to a specific 3rd party video provider
 * @return
 *   an array of strings requested by various admin and other forms
 *   'name' => the translated name of the provider
 *   'url' => the url to the main page for the provider
 *   'settings_description' => a description of the provider that will be posted in the admin settings form
 *   'supported_features' => an array of rows describing the state of certain supported features by the provider.
 *      These will be rendered in a table, with the columns being 'Feature', 'Supported', 'Notes'.
 */
function emvideo_animoto_info() {
  $features = array(
    array(t('Autoplay'), t('Yes'), ''),
    array(t('RSS Attachment'), t('No'), ''),
    array(t('Thumbnails'), t('No'), t('')),
  );
  return array(
    'provider' => 'animoto',
    'name' => t('Animoto'),
    'url' => EMVIDEO_ANIMOTO_MAIN_URL,
    'settings_description' => t('These settings specifically affect videos displayed from !provider.', array('!provider' => l(t('Animoto.com'), EMVIDEO_ANIMOTO_MAIN_URL))),
    'supported_features' => $features,
  );
}

/**
 *  Implements hook_emvideo_PROVIDER_settings
 */
function emvideo_animoto_settings() {
  $form = array();

  return $form;
}

/**
 * Implements hook_emvideo_PROVIDER_extract
 * this is called to extract the video code from a pasted URL or embed code.
 * @param $code
 *   an optional string with the pasted URL or embed code
 * @return
 *   either an array of regex expressions to be tested, or a string with the video code to be used
 *   if the hook tests the code itself, it should return either the string of the video code (if matched), or an empty array.
 *   otherwise, the calling function will handle testing the embed code against each regex string in the returned array.
 */
function emvideo_animoto_extract($embed = '') {
  if ($embed && preg_match('@' . EMVIDEO_ANIMOTO_PLAYER_PAGE_BASE_PATTERN . '([A-z0-9]*)@i', $embed, $matches)) {
    return $matches[1];
  }
  return FALSE;
}

/**
 * Implements hook_emfield_PROVIDER_data
 *
 * provides an array to be serialised and made available with $item elsewhere
 */
function emvideo_animoto_data($field, $item) {
  // Initialize the data array.
  $data = array();
  $data['emvideo_animoto_version'] = 1;

  // Gather info about the item's raw flash video.
  $url = EMVIDEO_ANIMOTO_PLAYER_URL . '&f=' . $item['value'];

  $response = emfield_request_header('animoto', $url);

  if ($response->code == 200) {
    $data['flash']['url'] = $url;
    $data['flash']['size'] = $response->headers['Content-Length'];
    $data['flash']['mime'] = $response->headers['Content-Type'];
  }

  return $data;
}


/**
 * Implements hook_emvideo_PROVIDER_embedded_link($video_code)
 * returns a link to view the video at the provider's site.
 *  @param $video_code
 *    The string containing the video to watch.
 *  @return
 *    A string containing the URL to view the video @ the original provider site.
 */
function emvideo_animoto_embedded_link($video_code) {
  return EMVIDEO_ANIMOTO_PLAYER_PAGE_BASE . $video_code;
}


/**
 *  Implements  hook_emvideo_PROVIDER_video
 *  This actually displays the full/normal-sized video we want, usually on the
 *  default page view.
 *  @param $embed
 *    The video code for the video to embed.
 *  @param $width
 *    The width to display the video.
 *  @param $height
 *    The height to display the video.
 *  @param $field
 *    The field info from the requesting node.
 *  @param $item
 *    The actual content from the field.
 *  @return
 *    The html of the embedded video.
 */
function emvideo_animoto_video($embed, $width, $height, $field, $item, $node, $autoplay) {
  $output = theme('emvideo_animoto_flash', $item, $width, $height, $autoplay);
  return $output;
}

/**
 *  Implements hook_emvideo_PROVIDER_video
 *
 *  This actually displays the preview-sized video we want, commonly for the
 *  teaser.
 *  @param $embed
 *    The video code for the video to embed.
 *  @param $width
 *    The width to display the video.
 *  @param $height
 *    The height to display the video.
 *  @param $field
 *    The field info from the requesting node.
 *  @param $item
 *    The actual content from the field.
 *  @return
 *    The html of the embedded video.
 */
function emvideo_animoto_preview($embed, $width, $height, $field, $item, $node, $autoplay) {
  $output = theme('emvideo_animoto_flash', $item, $width, $height, $autoplay);
  return $output;
}

/**
 * The embedded flash displaying the Animoto.com video.
 */
function theme_emvideo_animoto_flash($item, $width, $height, $autoplay) {
  $output = '';

  $oembed_url = EMVIDEO_ANIMOTO_OEMBED_BASE;
  $oembed_url .= '&url=' . $item['embed'] . '&maxwidth=' . $width . '&maxheight=' . $height . '&volume=50';
  $response = drupal_http_request($oembed_url);
  if (isset($response) && $response->code == '200') {
    $video_info = json_decode($response->data);
    $output .= $video_info->html;
    if($autoplay) {
      $output = str_replace('&options=', '&options=autostart', $output);
    }
  }
  else {
    $output .= t("Error retrieving video embed code, please use !link to view the video instead.", array('!link' => l(t('this link'), $item['embed'], array('attributes' => array('target' => '_blank')))));
  }

  return $output;
}

/**
 * Implements hook_emfield_subtheme().
 *
 * This returns any theme functions defined by this provider.
 */
function emvideo_animoto_emfield_subtheme() {
  $themes = array(
    'emvideo_animoto_flash'  => array(
      'arguments' => array('item' => NULL, 'width' => NULL, 'height' => NULL,
        'autoplay' => NULL),
        'file' => 'providers/animoto.inc',
        'path' => drupal_get_path('module', 'media_animoto'),
      )
  );
  return $themes;
}
